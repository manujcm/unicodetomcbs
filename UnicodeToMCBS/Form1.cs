﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UnicodeToMCBS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string inputUnicode = this.textBox1.Text;
                byte[] bytes = Encoding.GetEncoding(932).GetBytes(inputUnicode);
                //string sbytes = BitConverter.ToString(bytes);
                
                
                string outputJapan = "";               
                int i = 0;
                //while( i < bytes.Length - 1 )
                do
                {
                    string b1 = bytes[i].ToString("X");
                    string b2;
                    if (i + 1 == bytes.Length)
                        b2 = "00";
                    else
                        b2 = bytes[i + 1].ToString("X");
                    string s = "0x" + b2 + b1 + ", ";
                    outputJapan += s;

                    i += 2;
                } while (i < bytes.Length - 1);

                this.textBox2.Text = outputJapan;
                this.textBox3.Text = bytes.Length.ToString();

                //this.textBox2.Text = sbytes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }        
    }
}
